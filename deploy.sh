#/bin/bash
docker build . -t php-blog-ci3
docker image tag php-blog-ci3 10.217.177.144:5000/dev/php-blog-ci3:latest 
docker push 10.217.177.144:5000/dev/php-blog-ci3:latest --disable-content-trust

docker run -it -d -p 5551:80 --name=php-blog 10.217.177.144:5000/dev/php-blog-ci3:latest